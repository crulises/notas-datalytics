## Disclaimer

**Esperen encontrar errores ortográficos y falta de cohesión de ves en cuando,
eso por tratar de escribir lo más rápido posible.**

# Info.

- Fecha para el próximo encuentro 15/10
- Puede que durante el documento se encuentren marcas de tiempo, estas marcas
  hacen referencia a [este video](https://www.youtube.com/watch?v=FjOQ84kY0u8),
  el cuál es una grabación de la charla

# Resumen general

- ¿Cuáles son los valores del agilismo?
  > No es mas rápido, es mas flexible.
  - Entrega de valor continua
    > esto por el hecho de que cada Sprint tiene como objetivo incrementar el
    > valor del producto mediante el Incremento de Producto (IP).
  - Flexibilidad
    > Porque el Product Backlog (PB) está abierto a recibir cambios. Aunque se
    > debe aclarar que el Sprint Backlog (SB) no recibe cambios, una vez que se
    > compromete en un sprint con el SB, ese es el Sprint, no se puede agregar
    > nada y si se saca algo es porque se cancela.
  - Calidad
    > Cada persona del equipo se compromete con lo que tiene que entregar, lo
    > cual lleva a una mejora en la calidad del producto.
  - Equipo
    > Tan fundamental que se designa a alguien en el rol de Scrum Master (SM)
    > que tiene como rol mejorar el equipo, haciendo las retrospectivas que
    > apuntan a la mejora continua, lo cual tiene como objetivo hacer al equipo
    > cada vez mejor
  - Mejora continua
- Roles y Responsabilidades
  - Cada uno tiene que saber que función cumple
  - Product Owner: quien tiene que saber adonde vamos, que se quiere lograr, y
    es el que tiene la palabra final.
  - Development Team: el que trabaja durante todo el desarrollo.
  - Business Owner: dueño de la plata.
- Proyecto Scrum
  - Product Backlog: todo lo que se desea y se quiere.
  - Sprint Backlog: lo que se hace en este sprint, se define en el Sprint Planning.
  - Sprint: el proceso en si mismo del desarrollo, que termina con un
    Incremento de Producto, que el Product Owner presenta en la Sprint Review.
  - Daily: Reunión diaria que en donde se comunica que se hizo ayer, que se va
    a hacer hoy y cuales son los impedimentos que se tienen.
  - Para finalizar el equipo, junto al Scrum Master y el Product Owner hacen una
    retrospectiva, para mejorar el siguiente sprint.

# Introducción

- Comentarios: acá Claudio habló de que se va a ver con respecto a los
  proyectos ágiles teniendo especial énfasis en las experiencias propias que
  se obtuvieron en Datalytics, por ejemplo, menciona cuestiones que se puedan
  dar en un entorno multi-geográfico (entre otros.)

- La charla va a seguir estas lineas generales:
  - Entender por que los proyecto de Data Analytics fallan.
  - ¿Qué es ser ágil?
    - Dentro de esto de las metodologías ágiles
      - Conceptos, Roles.
  - Repaso final.

# Inicio

- Comentario: muchas de las herramientas dentro de las ciencias de la
  computación, se derivan de la ingeniería tradicional (civil, electrónica,
  mecánica, etc).
- Sin embargo, en las ciencias de la computación se tienen diferentes
  particularidades.
  - Varias de estas particularidades pueden aumentar la probabilidad
    de fracaso de un proyecto.
- Particularidades proyectos de Data Analytics:
- Los usuarios deben estar involucrados en el proyecto -- esto es
  FUNDAMENTAL.
- Es difícil encontrar gente con experiencia en el área.
- Los expertos pueden no tener el conocimiento necesario en un
  proyecto. Otra forma de explicar: el cliente puede no estar comunicando
  las necesidades de una manera efectiva. (comentario propio:
  supongo que esto mejora con la experiencia)
- Problemas de calidad de datos, cuestiones que ya se mencionaron
  anteriormente con el tema de campos incompletos, o que se
  completan con datos que no son verídicos
  > ejemplo propio: cuando compraba boletos de colectivo siempre me
  > rellenaban el DNI con el valor 1, lo mismo con los ejemplos vistos
  > en encuentros pasados cuando se hablaba de que las direcciones
  > en muchos clientes era la misma.
- Cambios constantes, esto relacionado con todo proyecto de
  Software en donde es muy común ver cambios a los requerimientos.
- Tiempos acotados, en donde la información tiene un atributo
  valioso que se denomina oportunidad, una pieza de información es
  útil cuando la misma es oportuna. La misma puede ser inútil si la
  recibo un mes después en vez de recibirla la semana entrante.
- Estándares

# ¿Por qué fallan los proyectos de DA?

Los proyectos de DA fallan principalmente por dos cuestiones: Parálisis por
análisis o Extinción por acción.

- Parálisis por análisis: Cuando se quiere el todo identificado, nos dedicamos
  sobre-analizar y mirar hasta lo mas mínimo de lo que se quiere hacer.
  - Documentos gigantescos.
  - Uno se pasa meses levantando información. Entendiendo lo que sucede
  - Muchas batallas con los clientes por conseguir el permiso por la
    especificación de lo que se va a hacer.
  - Falta de comprensión de lo que el cliente va a recibir. Este puede leer
    especificaciones, pero ¿Tiene una idea en la realidad de lo que se le está
    diciendo?
- Extinción por acción: acá uno sale a hacer, y sale a hacer las cosas sin
  saber que hacer. Se hace, se hace, se hace, pero en definitiva se hace todo
  sin un sentido por lo que termina siendo inútil.
  - Metas poco claras, lo que generalmente significa que se tiene que volver a
    rehacer algo
  - Sobrecarga al cliente con tareas (pedidos de aclaración, prueba de cosas
    que, al final no son útiles)
  - No se llega a una versión estable que pueda ser llevada a producción. Esto
    es consecuencia directa de no hacer un análisis detallado, se releva
    rápido, se especifica poco y se entiende menos. Falta un norte.

---

Entonces después de fallar ya sea por cualquiera de los motivos mencionados
anteriormente, uno encuentra un aliado en las metodologías. Particularmente
para la charla, las ágiles, ya que ayudan a llevar a proyectos a un "mejor
puerto" (esto no significa el éxito automático)

---

# Agilismo

> Retomamos el hecho de que se heredan cuestiones de la ingeniería.

- Una de las cuestiones heredadas aquí es la metodología Waterfall (cascada)
- Tradicional en los proyectos de ingeniería.
  > Ejemplo: nadie en sus sano juicio hace un puente o un edificio con una
  > metodología ágil. Una vez se tengan todos los requerimientos, se calcula
  > los recursos necesarios. Estos son fijos.
  - Se tiene un plan con lo que se estiman los recursos y el tiempo que se
    necesita para la construcción del puente (esto en sistemas se hereda de
    la ingeniería)
  - Cuando no se tienen los requerimientos definidos (sujetos a cambios) ahí no
    tiene mucho sentido el uso de cascada, ahí surgen las metodologías ágiles
    para ayudar. Acá lo que se tiene fijo son los recursos (personas) y el tiempo
    (15 días) => Se planea: que se puede hacer con los recursos en ese tiempo =>
    funcionalidad.
- ¿Cuándo utilizar cada uno?
  - Metodología y tecnologías conocidas => Waterfall
  - A medida que el escenario y/o las tecnologías tienden a lo desconocido =>
    Ágil

# 2001 Manifiesto ágil

> Origen en la década del 70' en la industria japonesa

- se tienen en cuenta los siguientes principios:
  - _Individuos e interacciones_ sobre procesos y herramientas.
  - _Software funcionando_ sobre documentación extensiva
  - _Colaboración con el cliente_ sobre negociación contractual
  - _Respuesta ante el cambio_ sobre seguir un plan
    > NO significa que se hace mas rápido porque no se documenta, significa si bien
    > se valora lo que se tiene a la derecha, se valora aún más lo que se tiene a la
    > izquierda. Además, existe un plan (como en waterfall), pero el mismo es
    > adaptable.
- 12 principios del agilísmo
- Sobre los 12 principios basamos los que son los valores del agilísmo.
  - Entrega de valor continua
  - Flexibilidad
  - Equipo
  - Mejora continua
    > Acá viene el ejemplo de la persona que "es flexible" y se pregunta, le pongo
    > a correr 100 metros o a hacer gimnasia artística. El agilismo no es mas
    > rápido, es mas flexible

# Familia ágil

- Es una mentalidad mas que una metodología.
- Existen varias metodologías ágiles, aunque para la charla y en lo que al
  curso respecta se pone el foco en SCRUM y un poco de Kanban

## Scrum

> Importante a notar: Claudio menciona que la charla se focaliza en Scrum
> porque en su experiencia, considera que scrum es la mas adecuada para llevar a
> cabo proyectos de analítica de datos

- Equipo: de 5 a 9 personas
  > sin embargo esto es flexible y va a depender de las necesidades
  > específicas de cada proyecto. En el ejemplo que se dió en la carla se hablo
  > de un banco que no tenía (en el equipo) un experto en seguridad de
  > sistemas.
- Roles específicos
  - Equipo: Scrum Master (SM); Business Owner (BO), Product Owner (PO),
    Development Team (DT).
  - **Scrum Master**
    > Especie de gurú, que tiene una parte de liderazgo y una parte de
    > contención, pero el foco esta en el equipo.
    - Actúa como agente de cambio (esto es lo que permite la mejora continua)
    - Elimina los impedimentos para que el equipo pueda trabajar.
      > estos impedimentos pueden ser relacionados con mediaciones que se tenga
      > con el PO o con el BO.
    - Prepara al PO para su tarea, familiarizada con el negocio, no con el
      proceso que se está desarrollando, por lo tanto, es responsabilidad del
      SM preparar al PO.
    - Ayuda al equipo en cuanto a lo externo
  - **Product Owner**
    > Este es el que tiene mas poder, por el hecho de que controla los recursos
    > (\$) es conveniente que sea el mismo que el BO, dado a que tiene que estar
    > seguro que cuenta con el dinero cuando algo se define por hacer. Si son
    > distintos, el equipo Scrum se abstrae y es pura responsabilidad del PO que
    > el BO proporcione el dinero necesario. TLDR; siempre queda en
    > responsabilidad del PO la disponibilidad de presupuesto para el proyecto
    - Persona que tiene que tener absolutamente claras cuales son las necesidades
      del negocio, y se encarga de comunicárselas al equipo.
      > estas necesidades son cambiantes, es responsabilidad del PO
      > comunicarlas de una manera efectiva al SM para mantener al proyecto
      > actualizado
    - Colaborar con los Stakeholders(personas interesadas en el proyecto).
      > estos tienen requerimientos que por lo general están desordenados, su
      > tarea es trabajar con ellos para ordenarlos, limpiarlos, depurarlas,
      > aclararlas y especificarlas.
    - Responsable de crear la visión del producto

---

> Acá surge una consulta por Flor: `Hola! Yo tengo una pregunta, como se relacionan los cargos del equipo scrum con las categorías/señorías de la empresa?`
>
> **Respuesta:** La jerarquía no se da muy bien en los proyectos que siguen SCRUM,
> es más, las entidades que tienen muy interiorizadas la aerarquía les cuesta mucho
> utilizar SCRUM. Si bien el PO y el SM tienen una especie de jerarquía por lo que
> son líderes en el grupo, ambos "valen lo mismo" tienen un "liderazgo
> horizontal".
> Cuando aparecen las preguntas "¿cuanto falta?" "¿cuánto vas a gastar en todo
> el proyecto?", ya se puede intuir que la metodología ágil no esta
> suficientemente interiorizada en la organización

---

- - Development Team
    - Autoorganización y equipo completo
      > Uno de los puntos que brinda la ese concepto autoorganización en la
      > metodología viene dada por la habilidad del equipo de desarrollo a
      > seleccionar (y hacerse responsable) por tareas que existen en el backlog.
    - Estos son los responsables de realizar los entregables.
    - Manejo y seguimiento
      > Lo que se va a desarrollar en la etapa tiene que estar claro y estos son
      > responsables de avanzar con eso

### Proyecto SCRUM

- Eventos (ceremonias)

  - Sprint Planning
  - Sprint
    > Nota: cuando terminó de pasar por todos los eventos y habiendo explicado
    > a cada uno, se da cuenta de que no incluyó este. De todas maneras brindó
    > una descripción superficial del mismo
  - Daily Scrum
  - Sprint Review
  - Sprint Retrospective
  - Sprint Refinement

- Sprint Planning

  - Quienes participan
    - Equipo; PO; SM
  - ¿Para que se hace?
    - Determinar que se va a hacer, cual será el alcance de la etapa a llevar a
      cabo (esta etapa se llama Sprint)
    - Determina cómo hacer el IP, y cuales son las tareas necesarias para
      llegar al entregable al final
      > (1:00:08) [Artifact] IP: Incremento de Producto, el propósito de un sprint es
      > agregar valor al negocio, esto mediante la adición de nueva funcionalidad
      > al producto construido en el sprint. Es potencialmente entregable,
      > Potencial porque en ultima instancia el PO decide si se despliega a
      > producción. Cumple con la definición de DoD
  - ¿Por qué se hace?
    - Conocer sobre qué se va a trabajar
    - Conocer lo suficiente para poder trabajar
  - Detalles
    - El equipo elige los items (presentes en el PB) con los que se compromete
      a completar en el sprint. Teniendo como referencia las cuestiones que
      aclaró el PO, pero se toma la libertad de arreglar las dependencias que
      el PO pueda haber pasado por alto.
      > (56:58min) [Artifact] PB: Product Backlog, Todo lo que tiene que ser hecho para
      > lograr el objetivo. Esto se mantiene en constante actualización dado a
      > que los recursos se renuevan. El responsable de esto es el PO. Está
      > refinado por todo el equipo.
    - Se crean el SB, esto en base a las tareas ya tomadas del PB
      > (58:42min) [Artifact] SB: Sprint Backlog, las tareas a construir, funcionalidad que el equipo
      > de desarrollo predijo que alcanzaría a adicionar al IP durante el Sprint.
      > Junto con las tareas que se necesitan para cumplir el objetivo de tener
      > la funcionalidad completa. Equipo es responsable por actualizar el
      > progreso
    - Se determinan todas las tareas que se van a incluir en el Sprint, cada
      una con su correspondiente estimación (realizada por el equipo
      completo)

---

> Acá surge una pregunta de Adrián Vázquez: `Al crear equipos, como se decide la asignación "óptima" de roles a los individuos?`
>
> **Respuesta:** depende mucho de que se va a hacer, cual es la tarea? Por
> ejemplo, en algún proyecto de analítica, se pueden asignar ingeniero de datos,
> expertos en visualización, arquitecto de datos, (cada uno toma un rol
> relacionado con lo que sabe hacer). Luego se tiene el PO que en el caso de
> Datalytics siempre suele ser el cliente, por ser el que conoce el negocio, y
> el SM es el que lidera. Recomienda que si cualquiera del equipo sabe hacer
> cualquier cosa, se puede decidir de manera arbitraria mediante un sorteo
>
> `Comentario`: a veces el SM se olvida que está en un proyecto, y hay que
> cumplir con el proyecto y no la metodología. Entonces se crea el SM-Líder de
> proyecto que es alguien que apunta hacia los mejores intereses del proyecto
> pero sin perder de vista la metodología

---

- Daily Scrum

  > También se la llama `stand-up meeting` porque tradicionalmente se la hace
  > de pie y de esa manera nadie se entretiene mucho

  - ¿Quienes participan?
    - Todos estan invitados pero solo integrantes del equipo pueden hablar
  - ¿Para que se hace?
    - Solo se hacen 3 preguntas.
      > A veces el SM interrumpe porque todos se van por las ramas
      - ¿Qué se hizo ayer?
      - ¿Qué voy a hacer hoy?
      - ¿Hay algún impedimento?
  - ¿Por qué se hace?
    - NO es para resolver problemas
    - NO es status del proyecto para el SM o PO
  - Detalles
    - Ayuda a eliminar reuniones innesesarias
    - Se toma un compromiso frente al equipo (autoregulación)
    - Se pueden disparar otras reuniones
      > Sujeto A dice algo en la reunión, esto que dice sujeto A tiene que ver
      > con sujeto B (ya sea por una inquietud, los caminos en el desarrollo del
      > producto se cruzan, etc), entonces esto da pie para que otra reunión se
      > dispare fuera de la daily.
    - Impedimentos
      - Interno al equipo, surge la necesidad de que alguien dentro del mismo
        se haga responsable de remover este impedimento
      - Externo al equipo, el PO o SM deben tomar la rienda y la tarea queda
        "en espera"

- Sprint Review
  > Reunión que se realiza al terminar el Sprint
  - ¿Quienes participan?
    - Todos están invitados
  - ¿Para que se hace?
    - Permite inspeccionar que se logro en el sprint
    - Presentación de nuevas funcionalidades/arquitecturas
  - ¿Por qué se hace?
    - Avances del proyecto, status
    - Informe de logros
  - Detalles
    > Lidera esto el PO
    - Se informa a TODOS (incluido los SH) acerca de los avances
      > SH: stakeholder
    - Se espera feedback de SH y usuarios
    - Se reorganiza el PB

---

> Acá surge una pregunta de Martín Rey: `Hola, en los items del product backlog respetan el modelo de software o se adaptan para analytics?`
>
> **Respuesta:** Si, básicamente es el mismo concepto que historias de usuario,
> son las épicas (mismo concepto adaptado a analytics).

---

> Acá surge una pregunta de Doris Hippler: `¿alguien pondera lo que dice el colaborador que va a hacer?` -> se reformula a ` ¿Es coherente con las tareas?`
>
> **Respuesta:** Si, el SM hace un seguimiento de las tareas, y si alguien dice
> que va a hacer algo que no tiene sentido, se le repregunta por que toma una
> tarea que no corresponde, o esta muy desviado de lo previsto.

---

> Acá surge otra pregunta de Emiliano García: `Product owner puede ser el mismo en varios equipos?`
>
> **Respuesta:** PO y SM si, el resto de los roles se recomienda que no. El PO
> puede estar en más de un equipo dependiendo de la carga que tenga el equipo,
> ver si el proyecto es muy demandante (ej. avanza muy rápido)

---

- Sprint Retrospective

  - Quienes participan
    - Todo el equipo SCRUM
    - Opcional: usuarios, clientes, SH
  - ¿Para que se hace?
    - Realizar una revisión de que funciona y que no funciona en el equipo.
      > Muy importante por el hecho de que estos problemas que se estén dando
      > en un equipo luego se convierten en obstáculos
  - ¿Por qué se hace?
    - Mejorar la eficiencia del equipo y la comunicación interna
      > el equipo consigue su máxima capacidad de delivery a partir de cuarto
      > sprint, esto es siguiendo la metodología y siguiendo la retrospectiva.
      > No se busca un tratamiento psicologíco, si por ahí se busca algo en lo
      > personal.
  - Detalles
    > Hay veces que alguien no se adapta, no porque sea mal profesional, sino
    > porque es humano. Se busca ser mas eficiente en un equipo, la solución
    > puede estar en cambiar a una persona de equipo.
    - 30' máximo
    - Se pregunta:
      1. ¿Que se tiene que empezar a hacer?
      2. ¿Que se tiene que dejar de hacer?
      3. ¿Que se tiene que seguir haciendo?
    - Para el primero y el segundo se suele definir un plan de acción para
      mejorar.

- Sprint Refinement

  > También llamado Product Backlog Grooming

  - Quienes participan
    - PO, SM
    - Roles: funcional, arquitectos
    - Opcional: todos
  - ¿Para que se hace?
    - Se analizan los requerimientos de forma detallada
    - Se dividen items con una mayor granularidad
    - Se vuelve a estimar los items existentes y se estiman nuevos
  - ¿Por qué se hace?
    - Sirve para mantener/actualizar el backlog
    - Ordenar el Backlog
    - Analizar el Sprint Planning
  - Detalles
    - Foco en cuanto a los items de Spring Futuros
    - Se trabaja para tener bien definidas las US
      > US: user stories
    - A cada US se le crea el DoR y DoD
    - IMPORTANTE: se asignan tareas y responsables para items que tienen
      indeficiones. Importante la colaboratividad

- Sprint
  > Proceso de desarrollo, es todo lo que se pone en el Product Backlog.
  > Mediante el Sprint Planning, lo del PB pasa a una Sprint Backlog y durante
  > el Sprint, el equipo se dedica a llevar a cabo todas estas tareas del
  > Sprint Backlog, para así poder lograr el IP (incremento de producto =
  > Entregable)

---

_Al finalizar los eventos, se termina teniendo los tres artefactos
(herramientas) de SCRUM,
Product Backlog, Sprint Backlog y el Product Increment_

---

### Algunas Definiciones

- DoR
  - Todo lo que debe resolverse antes de que la US o la Tarea pueda ser
    comprometida en el sprint. Varia en proyectos/equipos.
    > Ejemplo: no se puede tomar una tarea que no tenga definido lo casos de
    > prueba/estimación. Tiene que tener suficiente explicación para que se
    > entienda que es lo que hay que hacer.
- DoD
  - Todo lo que se va a considerar necesario para que una US sea considerada
    como terminada. Tambien varía entre equipos/proyectos.
    > Ejemplo: un desarrollo de algún proceso, ¿Cuándo está terminado? el
    > programador te dirá `cuando lo termine de codificar`, sin embargo esto
    > depende de otros factores. Por ejemplo que tenga que pasar por entornos
    > de prueba, ser probados por el usuario, etc

> Comentario Sofía: A partir de pensar estas cuestiones, lo copado acá, es que
> las tareas se desagregan en mas tareas, por lo que el nivel de granularidad es
> mayor y se tiene mas claro lo que se debe cumplimentar

### Profundizar conceptos

`Scrum está diseñado para avanzar rapidamente, esto involucra herramientas que permiten entregar un MVP (minimum viable product), invirtiendo el menor tiempo posible`

Algunas de las piezas mencionadas son.

- User Stories (US)
  - Funcionalidad que se puede desarrollar entre 4 - 40 horas
  - Existen US mas grandes que luego pueden ser granularizadas.
  - Se redactan con un formato tentativo específico, los detalles se incorporan
    luego.
  - Escrita en un lenguaje simple.
  - Describe un escenario de uso.
  - Contiene toda la info necesaria para que las personas sepan que hacer con
    la US. Si no, el equipo consultará con el PO, que si no cuenta con las
    respuestas las conseguirá con el BO, o los SH.
  - Técnicas (mnemotécnicas para saber que poner)
    - Las 3-C
      - Card (Descripción), Confirmation (Criterios de Aceptación), Conversation (Explicación)
    - INVEST
      - Independent (Que sea independiente)
      - Negotiable (Que se pueda negociar)
      - Valuable (Que de valor al negocio)
      - Estimable (Que se pueda estimar)
      - Small (Que sea pequeña)
      - Testable (Que se puedan probar)
- Estimaciones
  > tema muy controversial en todo proyecto
  - Enfocado en reducir el tiempo gastado en estimar.
    > La experiencia con esta tarea, nos adelanta que consume mucho tiempo, y
    > mientras mas se invierte en estimar, no se invierte en desarrollar.
  - Tiene que ser participativo
  - No tiene que ser perfecto, es una ESTIMACIÓN
  - Base para el compromiso del sprint
    > porque se acepta una tarea que se considera se puede cumplir durante la
    > duración del mismo. Para esto se vale de la estimación de cuan grande es
    > lo que se debe hacer
  - Métodos
    - Estimación por afinidad / Opinión del experto
      > Básicamente en este se tiene en cuenta la opinión de alguien con
      > experiencia.
    - 5 dígitos oscilantes
    - Talle de remera
  - Técnicas
    - Story points
      > es un numero no relacionado con horas
      - La gente normalmente utiliza una serie de valores como la serie de
        fibonacci para asignar la etiqueta de valor.
  - No es necesario representarlo en horas
- TimeBoxing
  - Sprint = Caja grande
  - US = Cajitas mas pequeñas
  - Concepto => `se ponen US en el Sprint hasta que se llene la caja grande con cajitas`
- Sprints
  - Tipos de Sprint
    - Sprint 0: Crea el entorno para hacer algo. Ejemplo: otorgar permisos, crear el
      entorno de desarrollo, definir primeras tareas del PB. Es algo así como
      el "cuando tengamos todo arrancamos"
    - Architectural Spike: "¿Che y si lo hacemos así? ¿Probamos en un sprint a
      ver que onda?"
    - Risk Based: Sprint para atender al riesgo de seguridad. Por ejemplo
      agujero de seguridad en la app
    - Hardening: Preparar el pasaje a producción del incremento del producto
    - Refactoring
- Releases

  - Varios sprints para llegar a un release.
    > se ve mucho en proyectos de analítica, esto por la naturaleza inherente
    > del mismo, acá los proyectos empiezan muy en el backend, entonces por unos
    > cuantos sprints probablemente no se tenga nada para mostrar por lo que un
    > release no tiene sentido

- Product Backlog

  - ¿Qué hacer para lograr un objetivo?
  - Se mantiene en constante renovación.
    - esto debido a la naturaleza cambienates de los requisitos
  - Refinada por el equipo
  - US tamaño adecuado para sprint

- Sprint Backlog

# Distribuidores de información

> Esta es una traducción de `Information Radiator`
>
> Concepto: Un lugar en donde uno puede revisar cómo va el proyecto. Papelitos
> y post-it en pizarrones, Trello, JIRA, DevOps, etc. Con la ventaja que los
> las herramientas informáticas tienen utilidades que generan indicadores
> automáticamente.

- Este concepto viene a dar respuesta a los reportes de status y la cantidad
  considerable de tiempo que se invierte creándolos.
- Indicadores
  - Burndown
    > lo que muestra es la curva de la carga de tareas (horas) que hay que
    > hacer en el Sprint y cómo se viene ejecutando.
    - Representación del trabajo que falta, sobre el tiempo
    - Útil para predecir cuándo se completará todo el trabajo, y monitoreo de
      avance.
  - Kanban
    > Pizarrón con los papelitos.
    >
    > Concepto: Tres columnas (por hacer, en progreso y terminado), aunque
    > pueden ser más dependiendo de las necesidades del equipo, y las
    > tareas pasan de columna en columna hasta estar finalizados.
    - Principios
      - Limitar el Trabajo en progreso
      - Dejar claras las políticas
      - Mejorar la colaboración
      - Requerimientos caóticos
        > cuando no se puede planear nada, las tareas vienen de manera caótica,
        > "a lo Poisson"

---

> Acá surge una pregunta de Martín Rey: `El architectural spike sería un sprint como para hacer en proyectos con algo o mucho de incertidumbre como para decidir si se avanza o no?`
>
> **Respuesta:** Si, o un proyecto con mucha incertidumbre o que hay mucha
> incertidumbre sobre un tema puntual. Entonces se hace totalmente necesaria
> bajar la incertidumbre.

---
