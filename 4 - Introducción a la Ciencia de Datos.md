```console
** El propósito de este documento es el de servir como índice para revisar la charla **
```

- Resumen y recapitulación de lo que se vio durante las charlas anteriores.

1. Equipo de Ciencia de Datos
   - Ingeniero de datos
   - Científico de datos
   - Business stakeholder

---

`Entender que la monetización de los diferentes productos que se basan en datos tiene mucha relación con la capacidad de integrar lo mencionado en 1`

---

2. Metodología general (CRISP)
   > También trata a CRISP como guías.
   >
   > Después de tratar este tema, se pasa a hablar del porcentaje de tiempo que
   > se debe invertir en cada etapa.
   - Conocimiento de negocio
   - Conocimiento de datos
   - Preparación
   - Análisis y modelado
     > Acá se habla de lo importante que es invertir el tiempo en los items
     > anteriores. Se habla del 80% del tiempo.
   - Evaluación
   - Implementación
3. Ciclos de vida de los productos de datos
   - Necesidades
   - Definición del alcance
   - Diseño de posibles soluciones
   - Prototipado del producto
   - Validación del producto
4. Metodología según AWS
5. Metodología según Datalytics
6. Analítica Avanzada
   - Descriptiva -- ¿Qué está pasando?
   - Predictiva -- ¿Qué va a pasar?
   - Prescriptiva -- ¿Qué necesito hacer?
     > Acá se vio el ejemplo de Netflix y el sistema de recomendación

---

> Acá además se se ve un ejemplo concreto aplicado a un `contact center`

---

7. Entrenamiento de modelos predictivos
8. Tipos de Aprendizaje
   > Cuenta un poco de que trata inteligencia artificial, machine learning y
   > deep learning
   - Supervisado
     - Ej: Decision Tree, Regression
   - No supervisado
     - Ej: Clustering

---

Conceptos importantes a anotar

```
- Garbage In
- Garbage Out
```

---

9. Comparación de algunas técnicas
10. Rendimiento vs Interpretación
11. Herramientas
    > Hablando de capas de datos, teniendo en cuenta las cuestiones vistas en
    > las charlas anteriores
